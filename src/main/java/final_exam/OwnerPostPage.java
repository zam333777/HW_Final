package final_exam;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class OwnerPostPage extends AbstractPage{

    @FindBy(xpath =  "//h2[normalize-space()='post1']")
    private WebElement post1;
    @FindBy(xpath =  "//div[@class='content svelte-tv8alb']")
    private WebElement content;
    @FindBy(xpath =  "//div[normalize-space()='description 1']")
    private WebElement description1;
    @FindBy(xpath =  "//h2[normalize-space()='post 6']")
    private WebElement post6;

    @FindBy(xpath =  "//h2[normalize-space()='post2']")
    private WebElement post2;
    @FindBy(xpath =  "//div[normalize-space()='description2']")
    private WebElement description2;

    @FindBy(xpath =  " //button[@id='create-btn']")
    private WebElement CreatePostButton;
    @FindBy(xpath =  "//div[@class='content svelte-tv8alb']")
    private WebElement content2;

    @FindBy(xpath =  "//i[@class='material-icons rotate-180 mdc-icon-button__icon']")
    private WebElement sortPostDesc;

    @FindBy(xpath =  "//i[@class='material-icons mdc-icon-button__icon mdc-icon-button__icon--on']")
    private WebElement sortPostAsc;

    @FindBy(xpath =  "///span[@class='svelte-1rc85o5']")
    private WebElement Home;

    @FindBy(xpath =  "//a[@class='svelte-d01pfs']")
    private WebElement NextPrevPage;
    @FindBy(xpath =  "//a[1]//img[1]")
    private WebElement Picture;
    @FindBy(xpath =  "//div[@class='container svelte-1pbgeyl']")
    private WebElement PictureDetail;


    @FindBy(xpath =  "  //img[@alt='post 6']")
    private WebElement PicturePost6;




    public OwnerPostPage(WebDriver driver) {
        super(driver);
    }


    public WebElement post1() {
       return post1;
    }

    public WebElement picture() {
        return Picture;
    }

    public WebElement picturePost6() {
        return PicturePost6;
    }
    public WebElement content() {
        return content;
    }
    public WebElement description1() {
        return description1;
    }
    public WebElement post2() {
        return post2;
    }
    public WebElement content2() {
        return content2;
    }
    public WebElement description2() {
        return description2;
    }

    public WebElement createPostButton() {
        return CreatePostButton;
    }

    public OwnerPostPage clickSortPostDesc() {
        sortPostDesc.click();
        return  this;
    }

    public OwnerPostPage clickSortPostAsc() {
        sortPostAsc.click();
        return  this;
    }

    public OwnerPostPage clickHome() {
        Home.click();
        return  this;
    }

    public OwnerPostPage clickNextPrevPage() {
        NextPrevPage.click();
        return  this;
    }

    public OwnerPostPage clickPicture() {
        Picture.click();
        return  this;
    }
    public WebElement getPictureDetail() {
        return PictureDetail;
    }



    public WebElement post6() {
        return post6;
    }
}
