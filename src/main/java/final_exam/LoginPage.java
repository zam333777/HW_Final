package final_exam;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends AbstractPage {

    @FindBy(xpath = "/html/body/div/main/div/div/div[1]/form/div[3]/button")
    private WebElement login;

    @FindBy(xpath = "//input[@type='text']")
    private WebElement usernamefield;

    @FindBy(xpath = "//input[@type='password']")
    private WebElement passwordfield;

    @FindBy(xpath = "//main[@class='svelte-1pbgeyl']")
    private WebElement blog;
    @FindBy(xpath = "//a[normalize-space()='Hello, fakeuser1']")
    private WebElement HelloUser;

    @FindBy(xpath = "//p[normalize-space()='Invalid credentials.']")
    private WebElement error401;


    public LoginPage(WebDriver driver) {
        super(driver);
    }

     public LoginPage clicklogin() {
        login.click();
        return this;
    }

    public LoginPage setUsernamefield(String username){
        usernamefield.sendKeys(username);
        return  this;
    }

    public LoginPage setPasswordfield(String password){
        passwordfield.sendKeys(password);
        return  this;
    }
    public boolean checkErrorInvalidCreds(){
        return error401.isDisplayed();
    }

    public boolean checkBlog(){
        return blog.isDisplayed();
    }

    public boolean checkHelloUser(){
        return HelloUser.isDisplayed();
    }
}
