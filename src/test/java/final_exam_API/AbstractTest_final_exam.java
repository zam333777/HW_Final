package final_exam_API;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeAll;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public abstract class AbstractTest_final_exam {

    static Properties prop = new Properties();
    private static InputStream configFile;
    private static String password;
    private static String username;

    private static String urlauth;
    private static String urlposts;
    private static String urlnotuserposts;
    private static String token;

    protected static ResponseSpecification responseSpecification;
    protected static RequestSpecification requestSpecification;


    @BeforeAll


    static void initTest() throws IOException {
        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
        configFile = new FileInputStream("src/main/resources/my.properties");
        prop.load(configFile);
        prop.load(configFile);

        urlauth = prop.getProperty("url_auth");
        urlposts=prop.getProperty("url_posts");

        urlnotuserposts=prop.getProperty("url_otuserposts");
        username = prop.getProperty("username");
        password =prop.getProperty("password");
        token=prop.getProperty("token");

        responseSpecification = new ResponseSpecBuilder()
                .expectResponseTime(Matchers.lessThan(4000L))
                .log(LogDetail.ALL)
                .build();

        requestSpecification = new RequestSpecBuilder()
                .log(LogDetail.ALL)
                .build();


        RestAssured.responseSpecification = responseSpecification;
        RestAssured.requestSpecification = requestSpecification;

    }

    public RequestSpecification getRequestSpecification(){
        return requestSpecification;
    }
    public static String getUsername() {
        return username;
    }

    public static String getUrlauth() {
        return urlauth;
    }
    public static String getUrlPosts() {
        return urlposts;
    }

    public static String getUrlNotUserPosts() {
        return urlnotuserposts;
    }

    public static String getPassword() {
        return password;
    }

    public static String getToken() {
        return token;
    }



}
