package final_exam_API;

import io.restassured.path.json.JsonPath;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

public class Test_API extends AbstractTest_final_exam{



    //////////////////////////////////AuthPage\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    @Test
    void correctDataForAuth(){
        JsonPath response = given()
                .formParam("username", getUsername())
                .formParam("password", getPassword())
                .when()
                .post(getUrlauth())
                .body()
                .jsonPath();
        assertThat(response.get("token"), equalTo (getToken()));
        assertThat(response.get("username"), equalTo (getUsername()));

    }
    @Test
    void WrongUsernameForAuth(){
        JsonPath response = given()
                .formParam("username", "1234")
                .formParam("password", getPassword())
                .when()
                .post(getUrlauth())
                .body()
                .jsonPath();
        assertThat(response.get("error"), equalTo ("Invalid credentials."));
        assertThat(response.get("code"), equalTo (401));
    }

    @Test
    void WrongPasswordForAuth(){
        JsonPath response = given()
                .formParam("username", getUsername())
                .formParam("password","123")
                .when()
                .post(getUrlauth())
                .body()
                .jsonPath();
        assertThat(response.get("error"), equalTo ("Invalid credentials."));
        assertThat(response.get("code"), equalTo (401));
    }


    //////////////////////////////////BlogNotUserPge\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

    @Test
    void CheckPostsNotOwner(){
        JsonPath response = given()
                .queryParam("owner", "notMe")
                .queryParam("sort","createdAt")
                .queryParam("order","ASC")
                .header("X-Auth-Token", getToken())
                .when()
                .get(getUrlPosts())
                .body()
                .jsonPath();
        assertThat(( response.getMap("data[1]").get("title")), equalTo ("пельмешки"));
        assertThat(( response.getMap("data[1]").get("description")), equalTo ("dfghjkljhvcxvbnm,nbcghkjlkljhgvc"));
        assertThat(( response.getMap("data[0]").get("title")), equalTo ("Русские пельмени"));
        assertThat(( response.getMap("data[0]").get("description")), equalTo ("Русские пельмени… А насколько русские они на самом деле? Ведь известно, что до 30-х годов XIX века п"));
    }
    @Test
    void CheckPostsNotOwnerWithoutToken(){
        JsonPath response = given()
                .queryParam("owner", "notMe")
                .when()
                .get(getUrlPosts())
                .body()
                .jsonPath();
        assertThat(( response.get("message")), equalTo ("Auth header required X-Auth-Token"));
       }

    @Test
    void CheckPostsNotOwnerWithoutQueryParams(){

        JsonPath response = given()
                .queryParam("sort", "")
                .queryParam("order","")
                .queryParam("page","1")
                .queryParam("owner", "notMe")
                .header("X-Auth-Token", getToken())
                .when()
                .get(getUrlPosts())
                .body()
                .jsonPath();
        assertThat(( response.getMap("data[1]").get("title")), equalTo ("пельмешки"));
    }

    @Test
    void CheckPostsNotOwnerNotRealPage(){
        JsonPath response = given()
                .queryParam("owner", "notMe")
                .queryParam("sort","createdAt")
                .queryParam("order","ASC")
                .queryParam("page","99999999999999999999999999999999999999999999999")
                .header("X-Auth-Token", getToken())
                .when()
                .get(getUrlPosts())
                .body()
                .jsonPath();
        response.prettyPrint();
        assertThat(( response.getMap("data[0]")), equalTo (null));
    }

    @Test
    void CheckPageCountNotOwner(){
        int tmpPage=99;
        JsonPath response = given()
                .queryParam("owner", "notMe")
                .queryParam("sort","createdAt")
                .queryParam("order","ASC")
                .queryParam("page",tmpPage)
                .header("X-Auth-Token", getToken())
                .when()
                .get(getUrlPosts())
                .body()
                .jsonPath();
        response.prettyPrint();
        assertThat(( response.getMap("meta").get("prevPage")), equalTo (tmpPage -1));
        assertThat(( response.getMap("meta").get("nextPage")), equalTo (tmpPage +1));
    }







    //////////////////////////////////BlogUserPage\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

    @Test
    void CheckPostsFromUserCorrectData(){
        JsonPath response = given()
                .queryParam("sort","createdAt")
                .queryParam("order","DESC")
                .queryParam("page","1")
                .header("X-Auth-Token", getToken())
                .when()
                .when()
                .get(getUrlPosts())
                .body()
                .jsonPath();
        assertThat(( response.getMap("data[1]").get("title")), equalTo ("post 6"));
    }


    @Test
    void CheckPostsFromOwnerWithMistakeInQueryParams(){
       JsonPath response = given()
                .queryParam("order","ACSE")
                .queryParam("page","****")
                .header("X-Auth-Token", getToken())
                .when()
                .get(getUrlPosts())
                .body()
                .jsonPath();
        assertThat(( response.get("message")), equalTo ("Server error. Dont worry we already know about it."));
        assertThat(( response.get("code")), equalTo (500));
    }


        @Test
        void CheckPostsFromOwner(){
            JsonPath response = given()
                    .queryParam("sort","createdAt")
                    .queryParam("order","ASC")
                    .queryParam("page","1")
                    .header("X-Auth-Token", getToken())
                    .when()
                    .get(getUrlPosts())
                    .body()
                    .jsonPath();
           assertThat(( response.getMap("data[1]").get("title")), equalTo ("post2"));
           assertThat(( response.getMap("data[1]").get("description")), equalTo ("description2"));
           assertThat(( response.getMap("data[0]").get("title")), equalTo ("post1"));
           assertThat(( response.getMap("data[0]").get("description")), equalTo ("description 1"));
        }


    @Test
    void CheckPostsFromOwner2Page(){
          JsonPath response = given()
                  .queryParam("sort","createdAt")
                  .queryParam("order","ASC")
                  .queryParam("page","2")
                  .header("X-Auth-Token", getToken())
                .when()
                .get(getUrlPosts())
                .body()
                .jsonPath();
          response.prettyPrint();
        assertThat(( response.getMap("meta").get("nextPage")), equalTo (null));
    }

    @Test
    void CheckPostsFromOwnerWithWrongToken(){
        JsonPath response = given()
                .queryParam("sort","createdAt")
                .queryParam("order","ASC")
                .queryParam("page","1")
                .header("X-Auth-Token", "1231qfsadfasdcfascasdasdweqweq")
                .when()
                .get(getUrlPosts())
                .body()
                .jsonPath();
        assertThat(( response.get("message")), equalTo ("No API token provided or is not valid"));
    }

    @Test
    void CheckPostsFromOwnerWithOutToken(){
        JsonPath response = given()
                .queryParam("sort","createdAt")
                .queryParam("order","ASC")
                .queryParam("page","1")
                .when()
                .get(getUrlPosts())
                .body()
                .jsonPath();
        assertThat(( response.get("message")), equalTo ("Auth header required X-Auth-Token"));
    }



}
