package Final_exam_UI;

import final_exam.LoginPage;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;

import java.time.Duration;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class Absrtract_for_UI {
    static WebDriver webDriver;
    private static Object driver;

    @BeforeAll
    static void setDriver(){
        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--incognito");
        //options.addArguments("--headless");
        options.addArguments("start-maximized");
        options.setPageLoadTimeout(Duration.ofSeconds(5));
        options.addArguments("--remote-allow-origins=*");


        webDriver = new ChromeDriver(options);
        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
        webDriver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
    }

    @BeforeEach
    void initMainPage(){
        Assertions.assertDoesNotThrow( ()-> getWebDriver().navigate().to("https://test-stand.gb.ru/login"),
                "Страница не доступна");
        Assertions.assertTrue(true);

    }

    @AfterAll
    public static void exit(){

        if(webDriver !=null) webDriver.quit();
        }

    public WebDriver getWebDriver(){
        return this.webDriver;
    }

public void SuccessLogin() {
    LoginPage loginPage = new LoginPage(getWebDriver());
    loginPage
            .setUsernamefield("fakeuser1")
            .setPasswordfield("74e7e51214")
            .clicklogin();
    Assertions.assertTrue(loginPage.checkHelloUser());

    }




}
