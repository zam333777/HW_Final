package Final_exam_UI;
import final_exam.OwnerPostPage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.devtools.idealized.Javascript;

public class Test_UserBlog extends Absrtract_for_UI{


    @Test
    void CheckUserPostsVisibleTest()  {
        OwnerPostPage ownerPostPage = new OwnerPostPage(getWebDriver());
        SuccessLogin();
        ownerPostPage
                .clickSortPostDesc()
                .clickSortPostAsc();
        Assertions.assertTrue(ownerPostPage.post1().isDisplayed());
        Assertions.assertTrue(ownerPostPage.post2().isDisplayed());
        Assertions.assertTrue(ownerPostPage.description1().isDisplayed());
        Assertions.assertTrue(ownerPostPage.description2().isDisplayed());
        }


    @Test
    void CheckCreatePostButton()  {
        OwnerPostPage ownerPostPage = new OwnerPostPage(getWebDriver());
        SuccessLogin();
        Assertions.assertTrue(ownerPostPage.createPostButton().isDisplayed());
        ownerPostPage.createPostButton().click();
        Assertions.assertEquals("https://test-stand.gb.ru/posts/create",getWebDriver().getCurrentUrl());
    }


    @Test
    void CheckGotoPost()  {
        OwnerPostPage ownerPostPage = new OwnerPostPage(getWebDriver());
        SuccessLogin();

        ownerPostPage
                .clickSortPostDesc()
                .clickSortPostAsc()
                .post1().click();
        Assertions.assertTrue(ownerPostPage.content().isDisplayed());
        Assertions.assertEquals("https://test-stand.gb.ru/posts/68827",getWebDriver().getCurrentUrl());
    }


    @Test
    void CheckSortPostButton() {
        OwnerPostPage ownerPostPage = new OwnerPostPage(getWebDriver());
        SuccessLogin();
        ownerPostPage
                .clickSortPostDesc();
        Assertions.assertEquals("https://test-stand.gb.ru/?sort=createdAt&order=DESC", getWebDriver().getCurrentUrl());
        ownerPostPage.clickSortPostAsc();
        Assertions.assertEquals("https://test-stand.gb.ru/?sort=createdAt&order=ASC", getWebDriver().getCurrentUrl());
    }


    @Test
    void CheckPageButton() {
        OwnerPostPage ownerPostPage = new OwnerPostPage(getWebDriver());
        SuccessLogin();
        ownerPostPage
                .clickNextPrevPage();
        Assertions.assertTrue(ownerPostPage.post1().isDisplayed());
        Assertions.assertEquals("https://test-stand.gb.ru/?page=2", getWebDriver().getCurrentUrl());
        ownerPostPage
                .clickNextPrevPage();
        Assertions.assertTrue(ownerPostPage.post6().isDisplayed());
        Assertions.assertEquals("https://test-stand.gb.ru/?page=1", getWebDriver().getCurrentUrl());
    }


    @Test
    void CheckPictureSizePreview() {
        long width;
        long height;
        OwnerPostPage ownerPostPage = new OwnerPostPage(getWebDriver());
        SuccessLogin();
        width = ownerPostPage.picture().getSize().getWidth();
        height = ownerPostPage.picture().getSize().getHeight();
        Assertions.assertEquals(((height/2)*3), width);
    }

    @Test
    void CheckPictureSizeInPost() {
        long width;
        long height;
        OwnerPostPage ownerPostPage = new OwnerPostPage(getWebDriver());
        SuccessLogin();
        ownerPostPage
                .clickPicture();
        width = ownerPostPage.getPictureDetail().getSize().getWidth();
        height = ownerPostPage.getPictureDetail().getSize().getHeight();
        Assertions.assertEquals(((height / 2) * 3), width);

    }










}
