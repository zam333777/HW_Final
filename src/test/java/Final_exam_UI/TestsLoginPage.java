package Final_exam_UI;

import final_exam.LoginPage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestsLoginPage extends  Absrtract_for_UI{
    @Test
    void LoginPageNullCredsTest() {
        LoginPage loginPage = new LoginPage(getWebDriver());
        loginPage
                .clicklogin();
        Assertions.assertEquals("https://test-stand.gb.ru/login",getWebDriver().getCurrentUrl());
        Assertions.assertTrue(loginPage.checkErrorInvalidCreds());
           }


    @Test
    void LoginPageCorrectCredsTest()  {
        LoginPage loginPage = new LoginPage(getWebDriver());
        loginPage
                .setUsernamefield("fakeuser1")
                .setPasswordfield("74e7e51214")
                .clicklogin();
         Assertions.assertTrue(loginPage.checkBlog());
         Assertions.assertTrue(loginPage.checkHelloUser());
         Assertions.assertEquals("https://test-stand.gb.ru/",getWebDriver().getCurrentUrl());
    }

    @Test
    void LoginPageWrongCredsTest() {
        LoginPage loginPage = new LoginPage(getWebDriver());
        loginPage
                .setUsernamefield("12353")
                .setPasswordfield("5858741")
                .clicklogin();
        Assertions.assertTrue(loginPage.checkErrorInvalidCreds());

    }


    @Test
    void LoginPageSpaceCredsTest() {
        LoginPage loginPage = new LoginPage(getWebDriver());
        loginPage
                .setUsernamefield(" ")
                .setPasswordfield(" ")
                .clicklogin();
        Assertions.assertTrue(loginPage.checkErrorInvalidCreds());

    }

    @Test
    void LoginPage2SymbolsCredsTest() {
        LoginPage loginPage = new LoginPage(getWebDriver());
        loginPage
                .setUsernamefield("/1")
                .setPasswordfield("5b58b532f0")
                .clicklogin();
        Assertions.assertFalse(loginPage.checkBlog());
    }


    @Test
    void LoginPage21SymbolsCredsTest() {
        LoginPage loginPage = new LoginPage(getWebDriver());
        loginPage
                .setUsernamefield("asdfghjklqwertyuygfvv")
                .setPasswordfield("56c11beaeab")
                .clicklogin();
        Assertions.assertFalse(loginPage.checkBlog());
    }

    @Test
    void LoginPageCyrillicSymbolsCredsTest() {
        LoginPage loginPage = new LoginPage(getWebDriver());
        loginPage
                .setUsernamefield("варнинг")
                .setPasswordfield("89b851bef1")
                .clicklogin();
        Assertions.assertFalse(loginPage.checkBlog());
    }

}
